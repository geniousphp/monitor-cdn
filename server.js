var request = require('request');
var config 	= require('config');

var i = 0;

setInterval(function() {
	var url = config.checks[i].url;
	request({
	  	uri: url,
	  	method: "GET",
	}, function(err, res, body) {
		if(err){
			//send email
			url = null;
			return console.error(err);
		}
		if(res.headers['cf-cache-status']){
			if(res.headers['cf-cache-status'] == 'HIT'){
				console.log('GET ' + url + ' Cache HIT');
			}
			else{
				//send email
				console.error('GET ' + url + ' Cache MISS');
			}
		}
		else{
			//send email
			console.error('GET ' + url + ' cf-cache-status header not found');
		}
		url = null;
	});
	i++;
	if(i >= config.checks.length) i = 0;
}, config.interval);